# Activate virtualenv
/home/off-duty-manager/.virtualenvs/tweepy/bin/source .virtualenvs/tweepy/bin/activate

#run server
/home/off-duty-manager/.virtualenvs/tweepy/bin/python /home/off-duty-manager/code/django_oil_tweets/show_oil_tweets/manage.py

# run grab the tweet automatically
/home/off-duty-manager/.virtualenvs/tweepy/bin/python /home/off-duty-manager/code/django_oil_tweets/show_oil_tweets/tweet_aware_input.py

# send a message to my whatsapp
/home/off-duty-manager/.virtualenvs/tweepy/bin/python /home/off-duty-manager/code/django_oil_tweets/show_oil_tweets/whatsapp.py


# update the git too
cd code/django_oil_tweets/show_oil_tweets

#check status
/usr/bin/git status

#add them locally
/usr/bin/git add .

#commit them
/usr/bin/git commit -am "more_tweets"

#push to gitlab
/usr/bin/git push origin master

#push to heroku
/usr/bin/git push heroku master

touch /home/off-duty-manager/code/django_oil_tweets/show_oil_tweets/done.txt
