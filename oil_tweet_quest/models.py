from django.db import models

# Create your models here.
# tweet id , author_id , text , created_at , source 
# user  id , name , username , created_at , verified 

class Tweep(models.Model):
	author_id = models.IntegerField()
	name      = models.TextField()
	start_date = models.DateTimeField()
	verified = models.BooleanField()

class Tweet(models.Model):
	Tweep = models.ForeignKey(Tweep, on_delete=models.CASCADE ,unique = False)
	text = models.TextField()
	created_at = models.DateTimeField()
	device = models.TextField()

